package edu.byu.hbll.callnumber;

import java.text.DecimalFormat;

/** Helper constants and methods shared by the provided CallNumber implementations. */
class CallNumberUtil {

  /** The DecimalFormat instance that will properly pad the subjectNumber for correct sorting. */
  static final DecimalFormat NUMBER_FORMATTER = new DecimalFormat("000000.############");

  static {
    NUMBER_FORMATTER.setParseBigDecimal(true);
  }

  /**
   * Helper method that trims the provided string or returns the empty string ("") if the provided
   * string is null. This method was developed independently of, but provides identical behavior to
   * the trimToEmpty method in in org.apache.commons.lang3.StringUtils.
   *
   * @param str the string to trim
   * @return the trimmed string (may be empty, but will never be null)
   */
  static String nullableTrim(String str) {
    return str != null ? str.trim() : "";
  }

  static String nullableTrimAndConcat(String... strings) {
    StringBuilder concat = new StringBuilder();
    for (String string : strings) {
      concat.append(nullableTrim(string));
    }
    return concat.toString();
  }
}
