package edu.byu.hbll.callnumber;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * CallNumber implementation representing the default system-generated call number assigned by
 * SirsiDynix Symphony.
 *
 * <p>In addition to satisfying the requirements of the CallNumber interface,
 * SymphonyDefaultCallNumber also extracts the catalog key and call sequence and makes them
 * available via class-specific methods.
 */
public class SymphonyDefaultCallNumber implements CallNumber {

  /** Serialization version. */
  private static final long serialVersionUID = 1L;

  /**
   * The regular expression used to parse an Symphony default call number into its component values.
   */
  public static final Pattern SYMPHONY_DEFAULT_CALLNUMBER_PATTERN =
      Pattern.compile("^\\s*XX\\((\\d+)\\.(\\d+)\\)\\s*$", Pattern.CASE_INSENSITIVE);

  /** The catalog key embedded in the call number. */
  private final int catalogKey;

  /** The call sequence embedded in the call number. */
  private final int callSequence;

  /** The human-friendly normalized representation of the Symphony default call number. */
  private final String normalization;

  /** The (not-human-friendly) sortable representation of the Symphony default call number. */
  private final String sortKey;

  /**
   * Constructs a new instance using the provided string as the template.
   *
   * @param str the call number string to use as the template
   * @throws NullPointerException if the provided string is null
   * @throws IllegalArgumentException if the provided string cannot be parsed as an Symphony default
   *     call number.
   */
  public SymphonyDefaultCallNumber(String str) {
    // Make sure that we are parsing something that looks like an Symphony default call number and
    // capture the groups
    Matcher matcher = SYMPHONY_DEFAULT_CALLNUMBER_PATTERN.matcher(str);
    if (!matcher.matches()) {
      throw new IllegalArgumentException(
          "Does not match Symphony Default Call Number pattern: " + str);
    }

    this.catalogKey = Integer.parseInt(matcher.group(1));
    this.callSequence = Integer.parseInt(matcher.group(2));

    this.normalization = buildNormalization();
    this.sortKey = buildSortKey();
  }

  /**
   * Builds and returns the normalized string representation of the call number.
   *
   * @return the normalized string representation
   */
  private String buildNormalization() {
    return "XX(" + catalogKey + "." + callSequence + ")";
  }

  /**
   * Builds and returns the sortable string representation of the call number.
   *
   * @return the sortable string representation
   */
  private String buildSortKey() {
    return "XX("
        + CallNumberUtil.NUMBER_FORMATTER.format(new BigDecimal(catalogKey + "." + callSequence))
        + ")";
  }

  @Override
  public int compareTo(CallNumber other) {
    return sortKey().compareTo(other.sortKey());
  }

  @Override
  public String sortKey() {
    return sortKey;
  }

  @Override
  public String toString() {
    return normalization;
  }

  @Override
  public int hashCode() {
    return sortKey.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    if (!sortKey.equals(((CallNumber) obj).sortKey())) {
      return false;
    }
    return true;
  }

  /**
   * Returns the catalog key.
   *
   * @return the catalog key
   */
  public int getCatalogKey() {
    return catalogKey;
  }

  /**
   * Return the sequential ID of the call number.
   *
   * @return the call sequence
   */
  public int getCallSequence() {
    return callSequence;
  }
}
