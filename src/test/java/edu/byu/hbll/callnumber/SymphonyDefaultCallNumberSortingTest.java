package edu.byu.hbll.callnumber;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Unit tests for SymphonyDefaultCallNumber. */
public class SymphonyDefaultCallNumberSortingTest {

  @Test
  public void byuTest1_wholeNumber() {
    List<SymphonyDefaultCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new SymphonyDefaultCallNumber("XX(12345.1)"));
    expectedOrder.add(new SymphonyDefaultCallNumber("XX(67890.0)"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }

  @Test
  public void byuTest1_decimalNumber() {
    List<SymphonyDefaultCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new SymphonyDefaultCallNumber("XX(12345.0)"));
    expectedOrder.add(new SymphonyDefaultCallNumber("XX(12345.1)"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }
}
