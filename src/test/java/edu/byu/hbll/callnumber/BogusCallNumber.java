package edu.byu.hbll.callnumber;

/**
 * An implementation of CallNumber that deliberately does not define the constructor
 * BogusCallNumber(String).
 *
 * <p>Attempting to use this implementation with CallNumberParser should result in an
 * IllegalArgumentException.
 */
public class BogusCallNumber implements CallNumber {

  private static final long serialVersionUID = 1L;

  @Override
  public String sortKey() {
    return "";
  }
}
