package edu.byu.hbll.callnumber;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Unit tests for DeweyCallNumber ordering/sorting. */
public class DeweyCallNumberSortingTest {

  /** Ordering is based on the guideline - nothing before something. */
  @Test
  public void illinoisTest1_nothingBeforeSomething() {
    List<DeweyCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new DeweyCallNumber("973 C375"));
    expectedOrder.add(new DeweyCallNumber("973 C375a"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }

  /** Ordering is based on the guideline - nothing before something. */
  @Test
  public void illinoisTest2_decimalOrdering() {
    List<DeweyCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new DeweyCallNumber("778.53092 L558r"));
    expectedOrder.add(new DeweyCallNumber("778.54 C38"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }

  /** File one decimal place at a time. */
  @Test
  public void illinoisTest3_cutterOrdering() {
    List<DeweyCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new DeweyCallNumber("973 C375a"));
    expectedOrder.add(new DeweyCallNumber("973 C38"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }

  /** File one numerical place at a time after each capital letter. */
  @Test
  public void illinoisTest4_cutterOrdering2() {
    List<DeweyCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new DeweyCallNumber("846R532 Og1962"));
    expectedOrder.add(new DeweyCallNumber("846R56 Oc"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }

  /** Consider numbers as a break (i.e. 'a' and 'a2' less than 'ar'). */
  @Test
  public void illinoisTest5_breakpoints() {
    List<DeweyCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new DeweyCallNumber("973 C73a"));
    expectedOrder.add(new DeweyCallNumber("973 C73a2"));
    expectedOrder.add(new DeweyCallNumber("973 C73ar"));
    expectedOrder.add(new DeweyCallNumber("973 C73ar2"));
    expectedOrder.add(new DeweyCallNumber("973 C735"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }

  /** File groups of letters together when they precede numbers. */
  @Test
  public void illinoisTest6_lettersGroup() {
    List<DeweyCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new DeweyCallNumber("328.01 H96e"));
    expectedOrder.add(new DeweyCallNumber("328.01 He26k"));
    expectedOrder.add(new DeweyCallNumber("328.01 Hi12k"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }

  /** File groups of letters together when they precede numbers in serial records. */
  @Test
  public void illinoisTest7_lettersGroupSerials() {
    List<DeweyCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new DeweyCallNumber("051 YO v.16"));
    expectedOrder.add(new DeweyCallNumber("051 YO2h"));
    expectedOrder.add(new DeweyCallNumber("051 YOC v.1"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }

  /** Different editions of the same work are shelved in the increasing order of their dates. */
  @Test
  public void illinoisTest8_editionOrder() {
    List<DeweyCallNumber> expectedOrder = new ArrayList<>();
    expectedOrder.add(new DeweyCallNumber("973 C37a"));
    expectedOrder.add(new DeweyCallNumber("973 C37a 1950"));
    expectedOrder.add(new DeweyCallNumber("973 C37a 1955"));

    TestUtil.assertCorrectOrder(expectedOrder);
  }
}
