package edu.byu.hbll.callnumber;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

/** Unit tests for CallNumberParser. */
public class CallNumberParserTest {

  /** CallNumberParser should fail to construct if no parsing target types are defined. */
  @Test
  public void shouldFailToConstructWithNoTarget() {
    assertThrows(NullPointerException.class, () -> new CallNumberParser());
  }

  /** CallNumberParser should fail to construct if any null parsing target types are defined. */
  @Test
  public void shouldFailToConstructWithOnlyNullTarget() {
    assertThrows(
        IllegalArgumentException.class, () -> new CallNumberParser((Class<CallNumber>) null));
  }

  /** CallNumberParser should fail to construct if any null parsing target types are defined. */
  @Test
  public void shouldFailToConstructWithAnyNullTarget() {
    assertThrows(
        IllegalArgumentException.class,
        () -> new CallNumberParser(LCCallNumber.class, (Class<CallNumber>) null));
  }

  /**
   * CallNumberParser should fail to construct if any parsing target type fails to implement a
   * constructor that accepts a single string as an argument.
   */
  @Test
  public void construct_bogus() {
    assertThrows(
        IllegalArgumentException.class,
        () ->
            new CallNumberParser(LCCallNumber.class, BogusCallNumber.class, DeweyCallNumber.class));
  }

  /** CallNumberParser should construct with a single parsing target type. */
  @Test
  public void shouldConstructWithSingleTarget() {
    new CallNumberParser(LCCallNumber.class);
  }

  /** CallNumberParser should construct with multiple parsing target types. */
  @Test
  public void shouldConstructWithMultipleTargets() {
    new CallNumberParser(LCCallNumber.class, DeweyCallNumber.class);
  }

  /**
   * CallNumberParser.SYMPHONY_STRICT should properly parse provided strings to the appropriate call
   * number implementations.
   */
  @Test
  public void SYMPHONY_STRICT_shouldParseLCCallNumber() {
    CallNumberParser parser = CallNumberParser.SYMPHONY_STRICT;
    CallNumber expected = new LCCallNumber("A153.3 .L534z");
    CallNumber actual = parser.parse("A153.3 .L534z");

    assertEquals(expected, actual);
  }

  /**
   * CallNumberParser.SYMPHONY_STRICT should properly parse provided strings to the appropriate call
   * number implementations.
   */
  @Test
  public void SYMPHONY_STRICT_shouldParseDeweyCallNumber() {
    CallNumberParser parser = CallNumberParser.SYMPHONY_STRICT;
    CallNumber expected = new DeweyCallNumber("153.3 L534z");
    CallNumber actual = parser.parse("153.3 L534z");

    assertEquals(expected, actual);
  }

  /**
   * CallNumberParser.SYMPHONY_STRICT should properly parse provided strings to the appropriate call
   * number implementations.
   */
  @Test
  public void SYMPHONY_STRICT_shouldParseSymphonyDefaultCallNumber() {
    CallNumberParser parser = CallNumberParser.SYMPHONY_STRICT;
    CallNumber expected = new SymphonyDefaultCallNumber("XX(123456.1)");
    CallNumber actual = parser.parse("XX(123456.1)");

    assertEquals(expected, actual);
  }

  /**
   * CallNumberParser.SYMPHONY_NONSTRICT should properly parse provided strings to the appropriate
   * call number implementations.
   */
  @Test
  public void SYMPHONY_NONSTRICT_shouldParseLCCallNumber() {
    CallNumberParser parser = CallNumberParser.SYMPHONY_NONSTRICT;
    CallNumber expected = new LCCallNumber("A153.3 .L534z");
    CallNumber actual = parser.parse("A153.3 .L534z");

    assertEquals(expected, actual);
  }

  /**
   * CallNumberParser.SYMPHONY_NONSTRICT should properly parse provided strings to the appropriate
   * call number implementations.
   */
  @Test
  public void SYMPHONY_NONSTRICT_shouldParseDeweyCallNumber() {
    CallNumberParser parser = CallNumberParser.SYMPHONY_NONSTRICT;
    CallNumber expected = new DeweyCallNumber("153.3 L534z");
    CallNumber actual = parser.parse("153.3 L534z");

    assertEquals(expected, actual);
  }

  /**
   * CallNumberParser.SYMPHONY_NONSTRICT should properly parse provided strings to the appropriate
   * call number implementations.
   */
  @Test
  public void SYMPHONY_NONSTRICT_shouldParseSymphonyDefaultCallNumber() {
    CallNumberParser parser = CallNumberParser.SYMPHONY_NONSTRICT;
    CallNumber expected = new SymphonyDefaultCallNumber("XX(123456.1)");
    CallNumber actual = parser.parse("XX(123456.1)");

    assertEquals(expected, actual);
  }

  /**
   * CallNumberParser.SYMPHONY_NONSTRICT should properly parse provided strings to the appropriate
   * call number implementations.
   */
  @Test
  public void SYMPHONY_NONSTRICT_shouldParseUnclassifiedCallNumber() {
    CallNumberParser parser = CallNumberParser.SYMPHONY_NONSTRICT;
    CallNumber expected = new UnclassifiedCallNumber("1234blah");
    CallNumber actual = parser.parse("1234blah");

    assertEquals(expected, actual);
  }
}
