package edu.byu.hbll.callnumber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

/** Unit tests for LCCallNumber construction and parsing. */
public class LCCallNumberCompositionTest {

  @Test
  public void construct_withSubjectLetterOnly() {
    LCCallNumber a = new LCCallNumber("PN");
    assertEquals("PN", a.getSubjectClass());
  }

  @Test
  public void construct_withSubjectNumber() {
    LCCallNumber a = new LCCallNumber("PN1995");
    LCCallNumber b = new LCCallNumber("PN 1995");

    assertEquals("PN", a.getSubjectClass());
    assertEquals(new BigDecimal("1995"), a.getSubjectNumber());
    assertTrue(a.equals(b));
  }

  @Test
  public void construct_withSubjectDecimal() {
    LCCallNumber a = new LCCallNumber("PN 1995.9");
    LCCallNumber b = new LCCallNumber("PN 1995 .9");

    assertEquals("PN", a.getSubjectClass());
    assertEquals(new BigDecimal("1995.9"), a.getSubjectNumber());
    assertTrue(a.equals(b));
  }

  @Test
  public void construct_withCutter1() {
    LCCallNumber a = new LCCallNumber("PN 1995.9.A3");
    LCCallNumber b = new LCCallNumber("PN 1995.9 .A3");

    assertEquals("PN", a.getSubjectClass());
    assertEquals(new BigDecimal("1995.9"), a.getSubjectNumber());
    assertEquals("A3", a.getCutter1());
    assertTrue(a.equals(b));
  }

  @Test
  public void construct_withCutter2() {
    LCCallNumber a = new LCCallNumber("PN 1995.9 .A3T667");
    LCCallNumber b = new LCCallNumber("PN 1995.9 .A3 T667");

    assertEquals("PN", a.getSubjectClass());
    assertEquals(new BigDecimal("1995.9"), a.getSubjectNumber());
    assertEquals("A3", a.getCutter1());
    assertEquals("T667", a.getCutter2());
    assertTrue(a.equals(b));
  }

  @Test
  public void construct_periodical() {
    LCCallNumber a = new LCCallNumber("Z 7164 .G7 N54 1972 vol.19");

    assertEquals("Z", a.getSubjectClass());
    assertEquals(new BigDecimal("7164"), a.getSubjectNumber());
    assertEquals("G7", a.getCutter1());
    assertEquals("N54", a.getCutter2());
    assertEquals("1972 vol.19", a.getPostCutter());
  }

  @Test
  public void construct_periodical2() {
    LCCallNumber a = new LCCallNumber("AY 64 .A1ay 1857");

    assertEquals("AY", a.getSubjectClass());
    assertEquals(new BigDecimal("64"), a.getSubjectNumber());
    assertEquals("A1ay", a.getCutter1());
    assertEquals("1857", a.getPostCutter());
  }

  @Test
  public void construct_withPostCutter() {
    LCCallNumber a = new LCCallNumber("PN 1995.9.A3T667 2004");
    LCCallNumber b = new LCCallNumber("PN 1995.9 .A3 T667 2004");

    assertEquals("PN", a.getSubjectClass());
    assertEquals(new BigDecimal("1995.9"), a.getSubjectNumber());
    assertEquals("A3", a.getCutter1());
    assertEquals("T667", a.getCutter2());
    assertEquals("2004", a.getPostCutter());
    assertTrue(a.equals(b));
  }

  @Test
  public void construct_fromNormalized1() {
    LCCallNumber a = new LCCallNumber("RC 793.2 .A6C4 2000");
    LCCallNumber b = new LCCallNumber(a.sortKey());

    assertTrue(a.equals(b));
    assertEquals(0, a.compareTo(b));
  }

  @Test
  public void construct_fromNormalized2() {
    LCCallNumber a = new LCCallNumber("DA 467 .K3x");
    LCCallNumber b = new LCCallNumber(a.sortKey());

    assertTrue(a.equals(b));
    assertEquals(0, a.compareTo(b));
  }

  @Test
  public void equality_identical() {
    LCCallNumber a = new LCCallNumber("RC 793.2 .A6C4 2000");
    LCCallNumber b = new LCCallNumber("RC 793.2 .A6C4 2000");
    assertTrue(a.equals(b));
    assertEquals(0, a.compareTo(b));
  }

  @Test
  public void equality_differentSpacing() {
    LCCallNumber a = new LCCallNumber("RC 793.2 .A6C4 2000");
    LCCallNumber b = new LCCallNumber("RC 793 .2 .A6 C4 2000");
    assertTrue(a.equals(b));
    assertEquals(0, a.compareTo(b));
  }

  @Test
  public void equality_notequivalent() {
    LCCallNumber a = new LCCallNumber("RC 793.2 .A6C4 2000");
    LCCallNumber b = new LCCallNumber("RC 793 .2 .A6 C4 2001");
    assertFalse(a.equals(b));
    assertNotEquals(0, a.compareTo(b));
  }
}
