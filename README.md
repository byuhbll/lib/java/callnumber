# Call Number Library

The Call Number library provides a flexible but robust way of working with call numbers, officially known as [library classifications](https://en.wikipedia.org/wiki/Library_classification).  At the heart of the library is the `CallNumber` interface and a universal `CallNumberParser`.  This library requires Java 8 or later.

## Usage

Add this library as a dependency to your project's pom.xml.

```xml
<dependency>
  <groupId>edu.byu.hbll</groupId>
  <artifactId>callnumber</artifactId>
  <version>1.1.0</version>
</dependency>
```

## Call Number Interface

Classes implementing `CallNumber` normally represent an entire library classification scheme; for example, the library comes pre-packaged with an `LCCallNumber` implementation (representing the [Library of Congress Classification](https://www.loc.gov/catdir/cpso/lcc.html)) and the `DeweyCallNumber` implementation (representing the [Dewey Classification](https://www.oclc.org/dewey)).  But this is not always the case, and specialized implementation can be created to manage institutional or ILS-specific quirks.  For example, the library also includes the `SymphonyDefaultCallNumber` implementation, which allows it to handle the default call number placeholders used in newly created catalog records in the popular [SirsiDynix Symphony ILS](http://www.sirsidynix.com/products/sirsidynix-symphony).

The `CallNumber` interface requires that implementations define a `sortKey` method.  `sortKey` returns a `String` that will order/sort correctly (since many library classifications have sorting rules that contradict normal `String` ordering which is based on the character set).  The `sortKey` response need not be human readable, but it must be non-null.

Further, each `CallNumber` implementation should be defined as an immutable, thread-safe value object, similar to `String`, `Integer`, or `URI`.  As value objects, implementations are expected to implement a `toString` method that returns a normalized, human-readable representation of the call number.  Additionally, they should override `hashCode` and `equals` such that for any two non-null `CallNumber` objects `a` and `b`, if `a.getClass().equals(b.getClass())` and `a.sortKey().equals(b.sortKey())`, then `a.equals(b)` is also TRUE.

An additional contractual requirement exists that requires that `CallNumber` implementations must provide a constructor that accepts a single `String` argument.  This is necessary for the implementation to be suitable as a target for the `CallNumberParser` described in the next section.

Finally, Users should also be aware that the `CallNumber` interface extends both `Comparable` and `Serializable`, and that a default implementation for `compareTo` (which compares the `sortKey` response) has been provided which should be suitable for most, if not all, implementations.

The following stub implementation may be used as a template for users wishing to create their own `CallNumber` implementations:

```java
public final class StubCallNumber {

  private final String normalization;
  private final String sortKey;

  public StubCallNumber(String str) {
    Objects.requireNonNull(str);
    // TODO: Do whatever validation and parsing as is necessary for this implementation...
    // TODO: Set this.normalization to the normalized, human-friendly form of the call number...
    // TODO: Set this.sortKey to the sortable version of the call number...
  }

  @Override
  public String sortKey() {
    return sortKey;
  }

  @Override
  public String toString() {
    return normalization;
  }

  @Override
  public int hashCode() {
    return sortKey.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    return Objects.equals(sortKey, ((CallNumber)obj).sortKey());
  }

}
```

## Call Number Parser

The main entry point for most usage of the library will be through the `CallNumberParser` class.  This class is immutable and thread-safe, and is built by providing a "varargs" array of `CallNumber` implementation classes to the constructor as parsing target types.  Subsequent `parse` calls to that instance of `CallNumberParser` will attempt to parse the provided `String` into the first matching target type, as demonstrated below:

```java
public static void main(String[] args) {
  CallNumberParser p = new CallNumberParser(LCCallNumber.class, DeweyCallNumber.class);

  // Will be an LCCallNumber, since it matches the format for that implementation, which is the
  // first in the list and thus takes highest priority.
  CallNumber a = p.parse("AB 123");

  // Will be a DeweyCallNumber, since it failed to match the first target in the list (LCCallNumber)
  // but matches the format for the second target type (DeweyCallNumber).
  CallNumber b = p.parse("123.45");

  // Will throw an IllegalArgumentException, since it matches the format of neither target type.
  CallNumber c = p.parse("bogus");
}
```

Note that a special `CallNumber` implementation is included with this library, `UnclassifiedCallNumber`, which will successfully parse against ANY string (including `null` and the empty string).  Users wishing to guarantee that the `CallNumberParser` will never throw an `IllegalArgumentException` (as demonstrated above) on a non-matching call number may do so by adding `UnclassifiedCallNumber.class` to the end of their target list when constructing the `CallNumberParser` instance:

```java
public static void main(String[] args) {
  // Note the addition of the UnclassifiedCallNumber to act as a plain-text failsafe if a
  // potential call number doesn't match any other pattern.  All strings, including null and
  // empty, will successfully parse to an UnclassifiedCallNumber.
  CallNumberParser parser =
      new CallNumberParser(LCCallNumber.class, DeweyCallNumber.class, UnclassifiedCallNumber.class);

  // Will return an UnclassifiedCallNumber, since it failed to match any previous target type.
  CallNumber callNumber = parser.parse("bogus");
}
```

For convenience, we have defined two pre-built `CallNumberParser` instances which may be used via the following
static references:

- `CallNumberParser.SYMPHONY_STRICT`
- `CallNumberParser.SYMPHONY_NONSTRICT`

See the class-level documentation for full details of these pre-built parsers.

## Developing This Project

Note: These instructions assume Docker and VS Code, with the Dev Containers extension, are installed.

Git clone the project locally and open it with VS Code. When prompted, reopen the project in a dev container. It will take a couple of minutes to download all of the dependencies and prepare the container for Java development. Proceed with development once the initialization is complete.

Tests can be run from within VS Code. Click the Testing icon and click "Run Tests".

To build and install the project locally in order to include it in another project for testing, run the following from the project directory on the host machine.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn -f /project/pom.xml -Duser.home=/var/maven clean package install
```

Building and deploying this project is handled by the GitLab pipeline. There are two possible destinations for the built project: the BYU HBLL internal Maven repository or the central Maven repository. Use the internal repository for non-public releases and the central repository for public open source releases. To instruct the pipeline to deploy to Maven Central, set the `MAVEN_CENTRAL_DEPLOY` CI/CD variable to `true`. Deploying to either the internal or central repository requires manually playing the deploy job. Note: Deployments to Maven Central are permanent.

## License

[License](LICENSE.md)